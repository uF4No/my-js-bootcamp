#Info
This is the list of topis that I think are worth reviewing to be confortable working with web applications built entirely in JavaScript. 

## General

#### You Dont Know JavaScript Books
  - [x] Up & Going
  - [x] Scope & Closures
  - [ ] This & Object Prototypes
  - [ ] Types & Grammar
  - [ ] Async & Performance
  - [ ] ES6 & Beyond

#### Review ES7 new features
  - [x] Async / Await
  - [ ] Other

### Webpack 4
  - [x] Intro and how to
  - [ ] How to lazy load different modules

## FrontEnd

#### Vue.js
  - [x] Vue Mastery: Intr to Vue.js course
  - [ ] Vue Mastery: review free advanced videos (reactivity system...)
  - [ ] Advanced Vue Component Design
  - [ ] Vue CLI & Vue UI
  - [ ] Vuex / Vue Router


## BackEnd

#### Node.js RisingStack Bootcamp
  - [x] Create a simple web application and make the test pass
  - [ ] Create a model for the Github API
  - [ ] Implement the database models
  - [ ] Implement helper functions for the database models
  - [ ] Create a worker process
  - [ ] Implement a REST API
  - [ ] Prepare your service for production  

## CI/CD

#### Jenkins
  - [ ] Install locally
  - [ ] Create pipelines to run tests and build Docker image
  
#### Docker tutorial
  - [x] Get Started
  - [ ] Develop with Docker
  - [ ] Configure Networking
  - [ ] Manage application data
  - [ ] Run your app in Production
  - [ ] Standards & Compliance

#### Kubernetes
  - [ ] Tutorial

#### OpenShift / AWS / GoogleCloud
  - [ ] How to deploy your app in different cloud services using Docker image.

## My Demos
  - [ ] Polish GoalsApp frontend coded in Vue.js
  - [ ] Write article explaining Promises, Async and Await
  - [x] Create a TDL API using Express. Saves data in MongoDB.
  - [ ] Build a reusable component for modals in GoalsApp.


## Final Project
Make a full project in JS. Must have:
#### General
  - [ ] Built using TDD approach using Mocha/Chai
  - [ ] CI/CD with Jenkins / Docker
  - [ ] Project sctructure will be separated by resources.
  - [ ] Use environment variables. No .env or config files
#### Backend
  - [ ] Expose it's own API using Express or Koa
  - [ ] Consume a third party API using request-native. (if not sure what to use, create a different service to expose an API )
#### Frontend
  - [ ] Responsive Front-End, but avoid using Bootstrap. Learn a new CSS framework or code it manually.
  - [ ] Code reusable Vue components using slots.
  - [ ] Use Webpack to compile and do lazy loads of the components.


  